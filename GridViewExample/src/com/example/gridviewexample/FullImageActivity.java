package com.example.gridviewexample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

public class FullImageActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.full_image_layout);
		Intent intent = getIntent();
		int position = intent.getIntExtra("id", 0);
		GridAdapter adapter=new GridAdapter(getApplicationContext());
		ImageView imageView = (ImageView) findViewById(R.id.full_image_view);
        imageView.setImageResource(adapter.array[position]);
	}

}
