package com.example.gridviewexample;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout.LayoutParams;
import android.widget.GridView;
import android.widget.ImageView;

public class GridAdapter extends BaseAdapter {
	Context context;
	// Keep all Images in array
	public Integer[] array = { R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk, R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk,R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk, R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk,R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk, R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk,R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk, R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk,R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk, R.drawable.bahubali, R.drawable.chennaiex,
			R.drawable.hpynewyr, R.drawable.dhoom, R.drawable.idiot,
			R.drawable.pk,

	};

	GridAdapter(Context context) {
		this.context = context;

	}

	@Override
	public int getCount() {

		return array.length;
	}

	@Override
	public Object getItem(int position) {
		
		return array[position];
	}

	@Override
	public long getItemId(int position) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView = new ImageView(context);
		imageView.setImageResource(array[position]);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
		return imageView;
	}

}
